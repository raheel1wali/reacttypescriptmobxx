import { Route, Switch, Redirect } from 'react-router-dom'
import MainView from '../views/Main/MainView';
const Routes = () => {
    return (
        <Switch>
            <Redirect exact from="/" to="/main" />
            <Route exact path="/main" component={MainView} />
        </Switch>
    );
}

export default Routes;