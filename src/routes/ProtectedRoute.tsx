import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { SessionStore } from 'stores/SessionStore/SessionStore'
import { AppProps } from 'common/AppProps';

/**
 * This router should be used onm routes that you must be logged in to see.
 * It will redirect you to the login page if you aren't logged in.
 * 
 * https://tylermcginnis.com/react-router-protected-routes-authentication/
 */
@inject('store')
@observer
class ProtectedRoute extends React.Component<AppProps, any> {
    // sessionStore: SessionStore
    constructor(props: AppProps) {
        super(props);
        // this.sessionStore = this.props.store.sessionStore
    }

    componentDidMount() { }

    componentWillUnmount() { }

    render() {
        // if (!this.sessionStore.isLoggedIn) {
        //     // If we are logged in -> redirect to the dashboard
        //     return (<Redirect to="/login" />);
        // }

        return (
            <Route {...this.props} />
        )
    }
}


export default ProtectedRoute;