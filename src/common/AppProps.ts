import { Theme } from '@material-ui/core/styles';
import {History, Location} from 'history';
import {match} from 'react-router-dom';

// The properties sent to the view by the framework when not injecting the stores

export interface AppProps {
    theme?: Theme;
    classes?: any;
    history?: History;
    location?: Location;
    match?: match;
    width?: string;
}