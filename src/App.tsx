import React from 'react';
import './App.css';

import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import Routes from 'routes/Routes';

const browserHistory = createBrowserHistory();

function App() {
  return (
    <Router history={browserHistory}>
      <Routes />
    </Router>
  );
}

export default App;
