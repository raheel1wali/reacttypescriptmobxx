import React from 'react';
//Appprops
import {AppProps} from 'common/AppProps'

class MainView extends React.Component<AppProps,any> {

    constructor(props: AppProps){
        super(props)
    }

    // componentDidMount() is invocked immediately after a component is
    // mounted (inserted int the tree). Initialization that requires DOM 
    // nodes should go here

    componentDidMount(){}

    // componentWillUnmount() is invoked immediately before component is unmounted and
    // destroyed. perform any necessary cleanup in this METHODS, such as invalidation Timers,
    // canceling network requests, or cleaning up any subscriptions that were created in

    componentWillUnmount(){}

    render(){
        return (
            <div>
                Hello World
            </div>
        )
    }
}


export default MainView;